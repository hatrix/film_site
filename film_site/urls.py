from django.conf.urls import patterns, include, url
from django.contrib import admin
from films import views

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', views.home),
    url(r'^films$', views.films),
    url(r'^series$', views.series),
    url(r'^aide$', views.show_help),
    url(r'^news$', views.news),
    url(r'^news/ajouts$', views.news_add),
    url(r'^news/suppressions$', views.news_suppr),
    url(r'^news/autres$', views.news_other),
    url(r'^admin/', include(admin.site.urls)),
)
