from django.contrib import admin
from films.models import Film, Links, News
from imdb import IMDB

class LinkInline(admin.StackedInline):
    model = Links
    extra = 1

class FilmAdmin(admin.ModelAdmin):
    list_display = ('title', 'language', 'quality', 'rating', 'imdb', 'film')
    readonly_fields = ('admin_image',)
    inlines = [LinkInline]

    def save_model(self, request, obj, form, change):
        if not obj.synopsis:
            number = 0
            if obj.language:
                try:
                    number = int(obj.language)
                except:
                    pass
            Imdb = IMDB(obj.title, number)
            Imdb.get_infos()
            obj.rating = Imdb.rating
            obj.title = Imdb.title
            obj.image = Imdb.title + ".jpg"
            obj.synopsis = Imdb.synopsis
            obj.imdb = Imdb.link
        obj.save()


class NewsAdmin(admin.ModelAdmin):
    list_display = ('message', 'category', 'date')

admin.site.register(Film, FilmAdmin)
admin.site.register(News, NewsAdmin)
