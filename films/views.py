from django.shortcuts import render
from django.http import HttpResponse
from films.models import Film, News

def home(request):
    return series(request)

def films(request):
    film_list = Film.objects.order_by('title')
    infos = dict({'film_list': film_list, 'display_film': True})
    return render(request, "films.html", infos)

def series(request):
    film_list = Film.objects.order_by('title')
    infos = dict({'film_list': film_list, 'display_film': False})
    return render(request, "films.html", infos)

def show_help(request):
    return render(request, "help.html")

def news(request):
    news = (News.objects.order_by('date')).reverse()
    infos = dict({'news': news})
    return render(request, "news.html", infos)

def news_add(request):
    news = (News.objects.filter(category="Ajout")).order_by('date').reverse()
    infos = dict({'news': news})
    return render(request, "news.html", infos)

def news_suppr(request):
    news = (News.objects.filter(category="Suppression"))\
            .order_by('date').reverse()
    infos = dict({'news': news})
    return render(request, "news.html", infos)

def news_other(request):
    news = (News.objects.filter(category="Autre")).order_by('date').reverse()
    infos = dict({'news': news})
    return render(request, "news.html", infos)
