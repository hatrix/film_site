import requests
import urllib
from django.conf import settings

class IMDB():
    def __init__(self, name, number):
        self.title = name
        self.ID = ""
        self.rating = 0
        self.synopsis = ""
        self.image = ""
        self.link = ""
        self.number = number

    def get_infos(self):
        r = requests.get("http://www.omdbapi.com/?s=" + self.title)
        json = r.json()
        if 'Response' not in json:
            length = len(json['Search'])
            if self.number < length:
                self.ID = json['Search'][self.number]['imdbID']
            else:
                self.ID = json['Search'][0]['imdbID']
            r = requests.get("http://www.omdbapi.com/?i=" + self.ID)
            json = r.json()
            
            self.title = json['Title']
            self.rating = json['imdbRating']
            self.synopsis = json['Plot']
            self.image = json['Poster']
            self.link = 'http://www.imdb.com/title/' + self.ID + "/"
            
            self.save_img()

    def save_img(self):
        urllib.urlretrieve(self.image, 
                           'films' + settings.STATIC_URL + "images/" 
                                   + self.title + ".jpg")
