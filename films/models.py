from django.db import models
from django.conf import settings

class Film(models.Model):
    title = models.CharField('title', max_length=50)
    language = models.CharField('language', max_length=10, blank=True)
    quality = models.CharField('quality', max_length=10, blank=True)
    rating = models.FloatField('rating', default=0, blank=True)
    film = models.BooleanField('film', blank=True)
    image = models.CharField('image', max_length=50, blank=True)
    synopsis = models.TextField('synopsis', blank=True)
    imdb = models.CharField('imdb', max_length=70, blank=True)
    
    def admin_image(self):
        return '<img src="%s"/>' \
               % (settings.STATIC_URL + "images/" + self.title + ".jpg")
    admin_image.allow_tags = True

    def __unicode__(self):
        if self.film:
            return "Film " + self.title
        return "Serie " + self.title

class Links(models.Model):
    film = models.ForeignKey(Film)
    name = models.CharField('name', max_length=50)
    short_name = models.CharField('short_name', max_length=30)
    link = models.CharField('link', max_length=300)

class News(models.Model):
    date = models.DateTimeField('date published')
    message = models.TextField('message')
    CAT_CHOICES = (
        ('Ajout', 'Ajout'),
        ('Suppression', 'Suppression'),
        ('Autre', 'Autre'),
    )
    category = models.CharField('category', max_length=20, choices=CAT_CHOICES)

    def __unicode__(self):
        return self.category + ", " + self.message

